/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.FuncionarioDAO;
import dto.FuncionarioDTO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */

 @WebServlet(name = "Controller", urlPatterns = {"/Controller"})

public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
 
    private static final String INSERIR_OU_EDITAR = "/funcionario.jsp";
    private static final String LISTAR = "/listarFuncionarios.jsp";
    private static final String BUSCAR = "/buscarFuncionarios.jsp";
    private FuncionarioDAO dao;

    public Controller() {
        super();
        dao = new FuncionarioDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String forward = "";
        
        /* INÍCIO DA DIFUSÃO */
        
        String action = request.getParameter("action");

        /* DELETAR */
            
        if (action.equalsIgnoreCase("delete")){            
            int id = Integer.parseInt(request.getParameter("id"));
            dao.deletar(id);
            forward = LISTAR;
            request.setAttribute("funcionarios", dao.getFuncionarios());
            
        /* EDITAR */
            
        } else if (action.equalsIgnoreCase("edit")){            
            forward = INSERIR_OU_EDITAR;
            int id = Integer.parseInt(request.getParameter("id"));
            FuncionarioDTO fun = dao.getFuncionarioPeloID(id);
            request.setAttribute("funcionario", fun);
            
        /* PROCURAR */
            
        } else if (action.equalsIgnoreCase("search")){
            forward = BUSCAR;
            String nome = request.getParameter("nome");            
            request.setAttribute("funcionarios", dao.getFuncionarioPeloNome(nome));
            
        /* LISTAR */
            
        } else if (action.equalsIgnoreCase("listarFuncionarios")){            
            forward = LISTAR;
            request.setAttribute("funcionarios", dao.getFuncionarios());
            
        } else {            
            forward = INSERIR_OU_EDITAR;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FuncionarioDTO fun = new FuncionarioDTO();
        fun.setNome(request.getParameter("nome"));
        fun.setIdade(Integer.parseInt(request.getParameter("idade")));
        fun.setTelefone(request.getParameter("telefone"));
        String id = request.getParameter("id");
        if(id == null || id.isEmpty())
        {
            dao.inserir(fun);
        }
        else
        {
            fun.setId(Integer.parseInt(id));
            dao.atualizar(fun);
        }
        RequestDispatcher view = request.getRequestDispatcher(LISTAR);
        request.setAttribute("funcionarios", dao.getFuncionarios());
        view.forward(request, response);
    }
 
}