/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import static dbu.Conexao.getConnection;
import dto.FuncionarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juliano.vieira
 */
public class FuncionarioDAO {
    
    /*  INSERIR */
    
    public static void inserir(FuncionarioDTO fun) {
        try {
            Connection con = getConnection();
            String sql = "insert into funcionario(nome,idade,telefone) values(?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, fun.getNome());
            ps.setInt(2, fun.getIdade());
            ps.setString(3, fun.getTelefone());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /* DELETAR */
    
    public static void deletar(int id){
        try{
            Connection con = getConnection();
            String sql = "delete from funcionario where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
            ps.close();
            con.close();
        }catch(SQLException ex){
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /* ALTERAR */
    
    public static void atualizar(FuncionarioDTO fun) {
        try {
            Connection con = getConnection();
            String sql = "update funcionario set nome = ?, idade = ?, telefone = ? where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, fun.getNome());
            ps.setInt(2, fun.getIdade());
            ps.setString(3, fun.getTelefone());            
            ps.setLong(4, fun.getId());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }      
    
    /* GET POR PARTE DO NOME */
    
    public static ArrayList<FuncionarioDTO> getFuncionarioPeloNome(String nome){        
        ArrayList<FuncionarioDTO> funcionarios = new ArrayList();
        try{
            Connection con = getConnection();
            String sql = "select * from funcionario where nome like ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "%" + nome + "%");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                FuncionarioDTO f = new FuncionarioDTO();
                f.setId(rs.getInt(1));
                f.setNome(rs.getString(2));
                f.setIdade(rs.getInt(3));
                f.setTelefone(rs.getString(4));
                funcionarios.add(f);
            }
        }catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionarios;        
    }
    
    /* GET PELO ID */
    
    public static FuncionarioDTO getFuncionarioPeloID(int id) {
        FuncionarioDTO funcionario = null;
        try {
            Connection con = getConnection();
            String sql = "select * from funcionario where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();            
            while (rs.next()) {         
                funcionario = new FuncionarioDTO(
                    rs.getInt("id"),
                    rs.getString("nome"),
                    rs.getInt("idade"),
                    rs.getString("telefone"));                
            }
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionario;
    }        
    
    /* GET TODOS FUNCIONARIOS */
    
    public static ArrayList<FuncionarioDTO> getFuncionarios() {
        ArrayList<FuncionarioDTO> funcionarios = new ArrayList();
        
         try {
            Connection con = getConnection();
            PreparedStatement ps = con.prepareStatement("select * from funcionario");
            ResultSet rs = ps.executeQuery();
            
             while (rs.next()) {
                FuncionarioDTO a = new FuncionarioDTO();
                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setIdade(rs.getInt(3));
                a.setTelefone(rs.getString(4));                
                funcionarios.add(a);
                
            }
            
            ps.close();
            con.close();
        } catch (Exception e) {            
            e.printStackTrace();
        }
         return funcionarios;
    }    
    
    
}