<%-- 
    Document   : listarFuncionarios
    Created on : 21/02/2017, 20:27:15
    Author     : juliano.vieira
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Listar todos funcionários</title>
</head>
<body>
    <p><a href="Controller?action=insert">Cadastrar novo funcionário</a></p>
    <p><a href="Controller?action=search">Buscar funcionário</a></p>
    
    <div>        
        <form method="GET" action='Controller' name="formBuscarFuncionario">        
            Nome : <input type="text" name="nome" value="" />             
            <input type="submit" value="Buscar" />
            <input type="hidden" value="search" name="action" />
        </form>        
    </div>
    
    <hr>
    
    <table border=0>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Idade</th>
                <th>Telefone</th>
                
                <th colspan=2>Ação</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${funcionarios}" var="funcionario">
                <tr>
                    <td><c:out value="${funcionario.id}" /></td>
                    <td><c:out value="${funcionario.nome}" /></td>
                    <td><c:out value="${funcionario.idade}" /></td>                    
                    <td><c:out value="${funcionario.telefone}" /></td>
                    <td><a href="Controller?action=edit&id=<c:out value="${funcionario.id}"/>">Editar</a></td>
                    <td><a href="Controller?action=delete&id=<c:out value="${funcionario.id}"/>">Deletar</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
</body>
</html>