/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  juliano.vieira
 * Created: 14/02/2017
 */

DROP DATABASE IF NOT EXISTS PROJETO;  

CREATE DATABASE PROJETO;  

USE PROJETO;

DROP TABLE 'funcionario';

CREATE TABLE funcionario (
  id mediumint(8) unsigned NOT NULL auto_increment,
  nome varchar(255) default NULL,
  idade mediumint default NULL,
  telefone varchar(100) default NULL,
  PRIMARY KEY (id)
) AUTO_INCREMENT=1;

INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Shaine",58,"758-4127"),("Mallory",48,"872-7717"),("Haley",41,"1-334-802-3103"),("Avram",44,"313-7675"),("Meghan",58,"683-4121"),("Dara",57,"1-813-956-2685"),("Angelica",29,"1-793-214-0664"),("Sylvester",59,"570-3399"),("Stephanie",52,"1-774-174-4885"),("Kennan",58,"358-2863");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Len",55,"179-3953"),("Miriam",30,"1-149-175-8489"),("Orson",29,"1-876-438-5440"),("Tanner",56,"667-5279"),("Hollee",29,"633-0013"),("Allegra",31,"1-172-248-0757"),("Ross",36,"279-3106"),("Clare",44,"172-7192"),("Harrison",45,"1-469-324-8020"),("Benedict",45,"815-4186");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Echo",41,"1-209-710-5495"),("Margaret",20,"207-8715"),("Calista",22,"1-339-293-5635"),("Amy",54,"1-347-129-5049"),("Chadwick",48,"1-660-321-6972"),("Audra",37,"1-474-138-9665"),("Ifeoma",38,"1-135-824-5494"),("Honorato",58,"824-2249"),("Xenos",34,"1-541-662-4837"),("Maris",42,"379-3403");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Harlan",35,"988-9325"),("Hop",31,"150-9563"),("Mikayla",59,"141-4364"),("Declan",44,"242-7139"),("Rinah",43,"638-9537"),("Rylee",59,"491-1553"),("Kane",23,"1-866-676-1606"),("Rowan",34,"914-8964"),("Leonard",27,"229-3875"),("Heather",26,"1-915-546-8963");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Joelle",37,"175-7344"),("Aaron",40,"1-958-641-1195"),("Shannon",23,"111-7481"),("Nola",47,"119-6510"),("Vivien",30,"1-627-941-3925"),("Darryl",38,"375-7570"),("Yasir",48,"425-0369"),("Cruz",56,"1-707-184-3786"),("Naida",37,"610-0446"),("Zelenia",53,"516-0714");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Cathleen",30,"667-1857"),("Beau",49,"1-448-701-1369"),("Guinevere",30,"1-291-746-3915"),("Stella",22,"1-590-119-4858"),("Jordan",47,"159-1768"),("Madonna",51,"898-3444"),("Molly",28,"702-2751"),("MacKensie",43,"1-329-859-4748"),("Isabella",27,"1-734-379-6881"),("Nyssa",42,"426-6603");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Kadeem",26,"653-5883"),("Hasad",53,"942-0188"),("Lester",28,"1-218-208-3151"),("Selma",45,"682-4113"),("Teagan",52,"1-700-729-8244"),("Chelsea",44,"334-5256"),("Paul",58,"1-176-436-8767"),("Astra",23,"1-275-310-5581"),("Illana",29,"1-499-596-7874"),("Imogene",41,"1-214-582-9910");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Karly",27,"1-105-782-3416"),("Charles",32,"701-4929"),("Alec",27,"275-8459"),("Ursula",48,"639-7975"),("Quinn",37,"1-287-190-7644"),("Quynn",59,"1-186-741-6364"),("Anika",59,"230-1894"),("Rosalyn",55,"564-1472"),("Timothy",43,"1-823-980-2377"),("Cruz",37,"1-884-106-3382");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Malachi",55,"363-4939"),("Lacy",55,"375-4289"),("Virginia",29,"1-168-212-4306"),("Tanner",30,"539-5062"),("Sonya",34,"358-4330"),("Jacob",32,"787-0228"),("Brynn",48,"1-210-179-6220"),("Libby",55,"1-340-413-4534"),("Kadeem",41,"1-221-577-3358"),("Francesca",51,"473-1871");
INSERT INTO funcionario (`nome`,`idade`,`telefone`) VALUES ("Conan",20,"1-837-541-3202"),("Mufutau",32,"1-201-732-0356"),("Christine",40,"234-0726"),("Fuller",37,"623-7252"),("Nicholas",49,"1-326-711-1629"),("Arthur",46,"1-471-201-8078"),("Graham",53,"1-197-102-6665"),("Amaya",26,"820-6355"),("Dana",51,"1-105-514-2293"),("Shannon",30,"846-5761");

SET @@global.time_zone = '+3:00';
QUIT;